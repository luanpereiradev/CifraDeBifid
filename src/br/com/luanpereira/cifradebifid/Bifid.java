package br.com.luanpereira.cifradebifid;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import static br.com.luanpereira.cifradebifid.BifidUtils.findByIndexes;
import static br.com.luanpereira.cifradebifid.BifidUtils.findByLetter;

/**
 * Classe criada em 16/06/17.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class Bifid {

    private static final char[] ALFABETO = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public static String encrypt(String text, long seed) {
        return encrypt(text, new Random(seed));
    }

    public static String decrypt(String bifid, long seed) {
        return decrypt(bifid, new Random(seed));
    }

    public static String encrypt(String text, Random rand) {
        char[] chars = ALFABETO.clone();
        BifidUtils.shuffle(chars, rand);

        text = Normalizer.normalize(text, Form.NFD);
        text = text.replaceAll("[^\\p{ASCII}]", "");
        text = text.replace(" ", "");
        text = text.toUpperCase();

        List<String> result = new ArrayList<>();
        for (String split : BifidUtils.split(text, 5)) {
            String lines = "", columns = "";
            for (char ch : split.toCharArray()) {
                try {
                    if (ch == 'J') ch = 'I';
                    int[] ints = findByLetter(chars, ch);
                    lines += Integer.toString(ints[0]);
                    columns += Integer.toString(ints[1]);
                } catch (NoSuchElementException e) {
                    // ignore
                }
            }
            result.add(lines);
            result.add(columns);
        }

        return String.join(" ", result);
    }

    public static String decrypt(String bifid, Random rand) {
        char[] chars = ALFABETO.clone();
        BifidUtils.shuffle(chars, rand);

        String result = "";
        String[] split = bifid.split(" ");
        for (int i = 0; i < split.length; i += 2) {
            if (i + 1 >= split.length) break;
            char[] lines = split[i].toCharArray();
            char[] columns = split[i+1].toCharArray();
            for (int k = 0; k < Math.min(lines.length, columns.length); k++) {
                try {
                    int line = Character.getNumericValue(lines[k]);
                    int column = Character.getNumericValue(columns[k]);
                    result += findByIndexes(chars, line, column);
                } catch (NoSuchElementException e) {
                    // ignore
                }
            }
        }

        return result;
    }

}
