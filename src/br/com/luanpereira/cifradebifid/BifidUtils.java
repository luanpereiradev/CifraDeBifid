package br.com.luanpereira.cifradebifid;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Random;

/**
 * Classe criada em 16/06/17.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class BifidUtils {

    public static String[] split(String text, int length) {
        Objects.requireNonNull(text, "Campo 'text' não pode ser nulo.");
        String[] split = new String[text.length() / length + (text.length() % length > 0 ? 1 : 0)];
        for (int i = 0, s = 0; s < text.length(); s += length) {
            split[i++] = text.substring(s, Math.min(text.length(), s + length));
        }
        return split;
    }

    public static void shuffle(char[] chars, Random random) {
        for (int i = chars.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            char letra = chars[index];
            chars[index] = chars[i];
            chars[i] = letra;
        }
    }

    public static char findByIndexes(char[] chars, int line, int column) {
        for (int i = 0; i < chars.length; i++) {
            int linha = (i / 5) + 1;
            int coluna = (i % 5) + 1;
            if (line == linha && column == coluna) {
                return chars[i];
            }
        }
        throw new NoSuchElementException();
    }

    public static int[] findByLetter(char[] chars, char letter) {
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == letter) {
                int linha = (i / 5) + 1;
                int coluna = (i % 5) + 1;
                return new int[]{linha, coluna};
            }
        }
        throw new NoSuchElementException();
    }

}
