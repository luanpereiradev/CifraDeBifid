package br.com.luanpereira.cifradebifid;

/**
 * Classe criada em 10/06/17.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 * Estado: incompleto
 */
public class Main {

    public static void main(String[] args) {
        String bifid = Bifid.encrypt("Teste", 5);
        System.out.println(" ");
        System.out.println("-------------------------------------");
        System.out.println("Bifid: " + bifid);
        System.out.println("-------------------------------------");
        System.out.println(" ");
        System.out.println("-------------------------------------");
        System.out.println("Decrypt: " + Bifid.decrypt(bifid, 5));
        System.out.println("-------------------------------------");
        System.out.println(" ");
    }

}
